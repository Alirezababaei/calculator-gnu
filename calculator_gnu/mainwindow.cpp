#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qmessagebox.h"
#include "form.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
bool jam;
bool zarb;
bool taqsim;
bool mnha;
double a,b,c;


void MainWindow::on_pushButton_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"1");
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"2");
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"3");
}

void MainWindow::on_pushButton_7_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"4");
}

void MainWindow::on_pushButton_6_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"5");
}

void MainWindow::on_pushButton_8_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"6");
}

void MainWindow::on_pushButton_11_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"7");
}

void MainWindow::on_pushButton_12_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"8");
}

void MainWindow::on_pushButton_9_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"9");
}

void MainWindow::on_pushButton_13_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+"0");
}

void MainWindow::on_pushButton_16_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+".");
}

void MainWindow::on_pushButton_4_clicked()
{
    a=ui->lineEdit->text().toDouble();
    jam=true;
    ui->lineEdit->clear();
}

void MainWindow::on_pushButton_5_clicked()
{
    a=ui->lineEdit->text().toDouble();
    mnha=true;
    ui->lineEdit->clear();
}

void MainWindow::on_pushButton_10_clicked()
{
    a=ui->lineEdit->text().toDouble();
    zarb=true;
    ui->lineEdit->clear();
}

void MainWindow::on_pushButton_15_clicked()
{
    a=ui->lineEdit->text().toDouble();
    taqsim=true;
    ui->lineEdit->clear();
}

void MainWindow::on_pushButton_14_clicked()
{
    b=ui->lineEdit->text().toDouble();
    if(jam==true){
c=a+b;
ui->lineEdit->setText(QString().number(c));


    }

else if (zarb){

        c=a*b;
        ui->lineEdit->setText(QString().number(c));
    }
    else if (mnha){

        c=a-b;
        ui->lineEdit->setText(QString().number(c));
    }

    else if (taqsim){

        c=a/b;
        ui->lineEdit->setText(QString().number(c));
    }
    jam=zarb=mnha=taqsim=false;

}

void MainWindow::on_pushButton_17_clicked()
{
    ui->lineEdit->clear();
}

void MainWindow::on_pushButton_18_clicked()
{
    Form *f=new Form();
    f->show();
}
